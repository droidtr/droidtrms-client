package org.droidtr.messaging;

public class Constants {
	public static final String SERVER_ADDRESS = "http://picjc.alwaysdata.net";
	public static final String SERVER_SEND_MESSAGE_ADDRESS = SERVER_ADDRESS+"/send.php";
	public static final String SERVER_LIST_MESSAGE_ADDRESS = SERVER_ADDRESS+"/list.php";
	public static final String SERVER_GET_MESSAGE_ADDRESS = SERVER_ADDRESS+"/write.php";
	public static final String SERVER_ADD_USER_ADDRESS = SERVER_ADDRESS+"/adduser.php";
	public static final String SERVER_REMOVE_USER_ADDRESS = SERVER_ADDRESS+"/deluser.php";
	public static final String SERVER_EDIT_USER_ADDRESS = SERVER_ADDRESS+"/edit.php";
	public static final String SERVER_CHECK_USER_ADDRESS = SERVER_ADDRESS+"/check.php";
	
	public static final String USER_PREFIX = "USER";
	public static final String MSG_PREFIX = "MSG";
	public static final String PIC_PREFIX = "PIC";
	public static final String AUD_PREFIX = "AUD";
	public static final String VID_PREFIX = "VID";
	
	public static final String SETTINGS_DB_NAME = "main";
	public static final String SETTINGS_USER_KEY = "user";
	public static final String SETTINGS_PASS_KEY = "passwd";
	
	public static final String SERVER_USER_KEY = "user";
	public static final String SERVER_PASS_KEY = "passwd";
	public static final String SERVER_RECEIVER_KEY = "member";
	public static final String SERVER_MSG_KEY = "message";
	public static final String SERVER_TIME_KEY = "time";
	public static final String SERVER_NAME_KEY = "name";
	
	public static final String SEPARATOR = "::";
}
