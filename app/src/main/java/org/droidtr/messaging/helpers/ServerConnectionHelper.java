package org.droidtr.messaging.helpers;

import android.content.*;
import com.android.volley.*;
import com.android.volley.Response.*;
import com.android.volley.toolbox.*;
import java.util.*;

import static com.android.volley.Request.Method.*;
import static org.droidtr.messaging.Constants.*;

class ServerConnectionHelper {
	
	static void sendStringRequest(Context ctx, String cmd, Map<String,String> m){
		ret = REQUEST_NOT_FINISHED;
		RequestQueue rq = Volley.newRequestQueue(ctx);
		StrReq sr = new StrReq(POST,cmd,new Listener<String>(){
				@Override
				public void onResponse(String response){
					ret = response.contains("OK") ? RESULT_OK : RESULT_FAILED;
					LoggingHelper.log(LoggingHelper.Type.DEBUG,response);
				}
			}, new ErrorListener(){
				@Override
				public void onErrorResponse(VolleyError error){
					ret = RESULT_SERVER_ERR;
					LoggingHelper.log(LoggingHelper.Type.ERROR,error.toString());
				}
			},m);
		rq.add(sr);
	}

	public int getRequestStatus(){
		return ret;
	}

	static int ret = -1;
	static final int REQUEST_NOT_FINISHED = -1, RESULT_OK = 0,
						RESULT_FAILED = 1, RESULT_SERVER_ERR = 2;
	
	static class StrReq extends StringRequest {
		Map<String,String> mParams;

		public StrReq(int method, String url, Response.Listener<String> listener,
				Response.ErrorListener errorListener, Map<String,String> params){
			super(method,url,listener,errorListener);
			mParams = params;
		}

		@Override
		protected Map<String, String> getParams(){
			return mParams;  
		}
	}
}
