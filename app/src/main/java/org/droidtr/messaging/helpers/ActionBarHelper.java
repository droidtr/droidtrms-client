package org.droidtr.messaging.helpers;

import android.app.*;
import android.graphics.drawable.*;
import android.view.*;
import android.widget.*;
import org.droidtr.messaging.*;

class ActionBarHelper {
	
	private ActionBarHelper(){}

	private static Activity ac;

	public static View createActionBar(Activity a, Drawable icon, String text, MenuItem... items){
		View v = (ac = a).getLayoutInflater().inflate(R.layout.action_bar,null);
		int d = (int) ac.getResources().getDimension(R.dimen.action_bar_height);
		v.setLayoutParams(new FrameLayout.LayoutParams(-1,d));
		ImageView ico = (ImageView) v.findViewById(R.id.action_bar_icon);
		ico.setImageDrawable(icon);
		TextView title = (TextView) v.findViewById(R.id.action_bar_title);
		title.setText(text);
		if(items != null){
			LinearLayout ll = (LinearLayout) v.findViewById(R.id.action_bar_menu);
			for(ActionBarHelper.MenuItem item : items){
				ll.addView(item.createMenuItem());
			}
		}
		return v;
	}

	public static View createActionBar(Activity a, int iconRes, String text, MenuItem... items){
		return createActionBar(a,a.getResources().getDrawable(iconRes),text,items);
	}

	public static View createActionBar(Activity a, int iconRes, int textRes, MenuItem... items){
		return createActionBar(a,a.getResources().getDrawable(iconRes),a.getString(textRes),items);
	}

	public static class MenuItem {

		Drawable mIcon;
		View.OnClickListener mOcl;

		public MenuItem(Drawable icon, View.OnClickListener ocl){
			mIcon = icon;
			mOcl = ocl;
		}

		private View createMenuItem(){
			ImageView iv = new ImageView(ac);
			int d = (int) ac.getResources().getDimension(R.dimen.action_bar_icon_size);
			iv.setLayoutParams(new LinearLayout.LayoutParams(d,d));
			iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
			iv.setImageDrawable(mIcon);
			iv.setOnClickListener(mOcl);
			iv.setPadding(d/5,0,d/5,0);
			return iv;
		}

	}
}
