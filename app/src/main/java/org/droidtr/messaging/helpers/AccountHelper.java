package org.droidtr.messaging.helpers;

import android.content.*;
import static org.droidtr.messaging.Constants.*;
import static org.droidtr.messaging.helpers.ServerConnectionHelper.*;
import java.util.*;

public class AccountHelper {
	
	Context ctx;
	
	public AccountHelper(Context c){
		ctx = c;
	}
	
	public void createAccount(String user, String pass, String name){
		Map<String,String> m = new HashMap<String,String>();
		m.put(SERVER_USER_KEY,user);
		m.put(SERVER_PASS_KEY,pass);
		m.put(SERVER_NAME_KEY,name);
		sendStringRequest(ctx,SERVER_ADD_USER_ADDRESS,m);
	}
	
	public void removeAccount(String user, String pass){
		Map<String,String> m = new HashMap<String,String>();
		m.put(SERVER_USER_KEY,user);
		m.put(SERVER_PASS_KEY,pass);
		sendStringRequest(ctx,SERVER_REMOVE_USER_ADDRESS,m);
	}
	
	public void checkAccount(String user, String pass){
		Map<String,String> m = new HashMap<String,String>();
		m.put(SERVER_USER_KEY,user);
		m.put(SERVER_PASS_KEY,pass);
		sendStringRequest(ctx,SERVER_CHECK_USER_ADDRESS,m);
	}
}
