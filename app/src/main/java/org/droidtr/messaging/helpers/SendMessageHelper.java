package org.droidtr.messaging.helpers;

import android.content.*;
import java.util.*;
import static org.droidtr.messaging.helpers.ServerConnectionHelper.*;
import static org.droidtr.messaging.Constants.*;

public class SendMessageHelper {
	private SharedPreferences sp = null;
	private Context ctx = null;

	public SendMessageHelper(Context c){
		ctx = c;
		sp = c.getSharedPreferences(SETTINGS_DB_NAME,c.MODE_PRIVATE);
	}

	public void sendTextMessage(String to, final String msg){
		final Map<String,String> data = new HashMap<String,String>();
		data.put(SERVER_USER_KEY,sp.getString(SETTINGS_USER_KEY,""));
		data.put(SERVER_RECEIVER_KEY,to);
		data.put(SERVER_PASS_KEY,sp.getString(SETTINGS_PASS_KEY,""));
		data.put(SERVER_MSG_KEY,msg);
		sendStringRequest(ctx,SERVER_SEND_MESSAGE_ADDRESS,data);
	}
}
