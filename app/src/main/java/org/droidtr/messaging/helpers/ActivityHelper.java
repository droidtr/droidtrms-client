package org.droidtr.messaging.helpers;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.view.*;
import android.widget.*;
import org.droidtr.messaging.*;

public class ActivityHelper {
	private ActivityHelper(){}
	
	// https://issuetracker.google.com/issues/36911528
	public static void bind(Activity a,View ... d){
		FrameLayout fl = (FrameLayout) a.findViewById(android.R.id.content);
		child = fl.getChildAt(0);
		child.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
				public void onGlobalLayout(){
					resizeIfPossible();
				}
			});
		flp = (FrameLayout.LayoutParams) child.getLayoutParams();
		if(d.length > 0){
			for(View v : d){
				fl.addView(v);
			}
		}
	}
	
	public static void bind(Activity a,boolean ab,ActionBarHelper.MenuItem... items){
		if(ab){
			View v = ActionBarHelper.createActionBar(a,R.drawable.ic_launcher,a.getTitle().toString());
			bind(a,v);
		} else bind(a);
	}

	private static View child;
	private static int uhp,uhn,diff,hwok;
	private static FrameLayout.LayoutParams flp;

	private static void resizeIfPossible(){
		uhn = computeUsableHeight();
		if (uhp != uhn){
			flp.height = isKeyboardOpen() ? hwok - diff : hwok;
			child.requestLayout();
			uhp = uhn;
		}
	}

	private static boolean isKeyboardOpen(){
		return (diff = (hwok = child.getRootView().getHeight()) - uhn) > (hwok/4);
	}

	private static int computeUsableHeight(){
		Rect r = new Rect();
		child.getWindowVisibleDisplayFrame(r);
		return (r.bottom - r.top);
	}
}
