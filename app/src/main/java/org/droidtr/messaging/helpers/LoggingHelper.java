package org.droidtr.messaging.helpers;

import android.util.*;

public class LoggingHelper {
	
	public enum Type {
		ERROR,
		DEBUG,
		INFO
	}
	
	public static void log(Type t,String msg){
		String x = "DroidTRMS::";
		switch(t){
			case ERROR:
				x += "ERROR";
				break;
			case DEBUG:
				x += "DEBUG";
				break;
			case INFO:
				x += "INFO";
				break;
		}
		Log.d(x,msg);
	}
	
}
